# README #

Hello everyone! This is HUDSON an open source framework for creating Adventure Games quickly and easily using Discord Bots.
This framework is in active development, and updates frequently with feature updates and optimizations.

### What is this repository for? ###

Release 0.1.0
Contains the first release for the HUDSON framework! Supports plater and room persistance.

If you want to see a demo game built on the engine, checkout the 0.1.0-Hudson-Engine-With-Demo branch
If you want the most recent (unsupported/unstable) release, check out the Dev branch.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

If you want to talk to us, feel free to stop by our website and shoot us an email!
splotchbotstudios.com

We would love to hear feature suggestions or comments from you guys!