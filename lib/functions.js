#!/usr/bin/env node

var relative = require('require-relative');
var JSONfn = require('json-fn');
var fs = require('fs');
var _ = require('underscore');
var engine = relative('./app.js');

module.exports = {

    //takes the player to be moved, and the room the player should be sent to
    //moves the player into the target room and reads the room state into the player
    rswitch: function(player, room){
      if (_.has(player.room, 'keep')) {
        this.saveRoom(player, room);
   	 }

   	 player.room = relative(room);

   	 if (_.has(player.room, 'keep')) {
        this.loadRoom(player, room);
   	 }

   	 player.room.enter(player);
    },
    //checks the keep state of any room the player is not currently in.
	 checkRoom: function (player, room){
		return fs.readFile(room + 'on', (err, data) => {
			if (err) {
				console.log(err);
				return {};
			} else {
				return JSONfn.parse(data);
			}
		});
	},
  //saves the keep value of the room that the player is in
    saveRoom: function (player, room){
        fs.writeFile(player.room.name + 'on', JSONfn.stringify(player.room.keep), (err) => {
            if (err) {
                player.channel.sendMessage(player.id + 'ERROR: ' + err);
            }
        });
    },
    //loads the state of that the player is in into the players memory
    loadRoom: function(player, room){
        fs.readFile(player.room.name + 'on', (err, data) => {
            if (err) {
                console.log(err)
                player.room.keep = player.room.keep;
            } else {
                try {
                    player.room.keep = JSONfn.parse(data);
                } catch(e) {
                    console.log(e);
                    player.room.keep = player.room.keep;
                }
            }
        })
    },
    //removes the player from the game
    killPlayer: function(player){
      delete engine.players[player.id];
    }
};
