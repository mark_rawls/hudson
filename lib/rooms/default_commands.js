#!/usr/bin/env node

var _ = require('underscore');

module.exports = {

  //TODO: Keep a log of defaults not overwritten when called.
  //the first word of a message are checked against an array like this, to determine if they have corresponding function

  // this is a list of all commands that should be present in every room. Some of them are simply here to
  // help you catch commands that should have been overwritten in your individual room files
  // other can have general helpfull information.

  commands: {
    //called everytime a player enters the room as per the rswitch function
    'ENTER': function(player){
      player.channel.sendMessage("If you are seeing this, your dev is a moron. OVERWRITE THE DEFAULT MORON");
    },
		'WHO': function(player) {
			player.channel.sendMessage("Oh god you really don't know? Okay here we go,\nYou are  " + player.id + "a member of the bridge crew on the USS Heuria, a Dreadnought Class Warship, taked with patroling the outer reach. We were attaked at 0800 hours last night, and the ship was nearly completely destroyed. You are among the last surviving crew.");
		},
		'WHAT': function(player) {
			player.channel.sendMessage("I am H.U.D.S.O.N. That stands for Heuria to User Directive and Systems Operation Network\nWhen this ship is operational I handle all of the autonomous systems.");
		},
		'HI': function(player) {
			player.channel.sendTTSMessage("We don't have time to be social.");
		},
    ///can be used to give the player a quick tutorial
    'HELP': function(player) {
      player.channel.sendMessage("You can MOVE in any cardinal direction...but I hope you already knew that. You can also LOOK at what's around you. Obviously. I mean, you have eyes.");
    },
    //this allow you to MOVE in a direction, and then parse following input to determine direction
    'MOVE': function(player, input) {
      //input is an array of the words following the main command word.
      //this shows how it can be used to expand the function of the base command.
      switch (input) {
        case 'S':
        case 'SOUTH':
        case 'DOWN':
          player.channel.sendMessage("if youre seeing this, your dev is a mororn. Overwrite the default moron!");

          break;
        case 'N':
        case 'NORTH':
        case 'UP':
          player.channel.sendMessage("if youre seeing this, your dev is a mororn. Overwrite the default moron!");
          break;
        case 'E':
        case 'EAST':
        case 'RIGHT':
          player.channel.sendMessage("if youre seeing this, your dev is a mororn. Overwrite the default moron!");
          break;
        case 'W':
        case 'WEST':
        case 'LEFT':
          player.channel.sendMessage("if youre seeing this, your dev is a mororn. Overwrite the default moron!");
          break;
        default:
          player.channel.sendMessage("if youre seeing this, your dev is a mororn. Overwrite the default moron!");
      }
    },
    //should provide general information about the room
    'LOOK': function(player) {
      player.channel.sendMessage("if youre seeing this, your dev is a mororn. Overwrite the default moron!");
    },
    //overflow for 'move', defaults back to move
		'GO': function(player, input) {
			if (_.has(player.room.commands, 'MOVE')) {
        player.room.commands.MOVE(player, input);
      }else {
        commands(player, input);
      }
		},
    //pulls a list of the players current inventory and spits it out in a list
		'INVENTORY': function(player) {
			let items = _.keys(player.inventory).join("\n");
			player.channel.sendMessage('You have:\n' + items);
		},
    //Same as inventory
    'CHECK': function(player) {
      let items = _.keys(player.inventory).join("\n");
			player.channel.sendMessage('You have:\n' + items);
    },
    //pick up whatever is in the room
    'TAKE': function(player) {
      player.channel.sendMessage('There\'s nothing to take, ' + player.id + ' you greedy meatbag');
    }
  }
};
