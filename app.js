#!/usr/bin/env node



var relative = require('require-relative'); //for file system traversal
var f = relative('./lib/functions.js'); // contains all helper functions for the engine
var discord = require('discord.js'); //its a discord bot
var fs = require('fs'); //file handling
var _ = require('underscore'); //
var players = {}; //All active players are stored in this array.
var bot = new discord.Client(); //create Discord bot object

bot.on('message', msg => {
	 let prefix = '<@238474854972522509> '; //the bots unique ID
	 author = '<@' + msg.author.id + '> '; //the message sender's unique ID
	 msg.content = msg.content.toUpperCase(); //Ignore casing of input, avoids confusion

	 //If the player is not referencing the bot, ignore the message
	 if (!msg.content.startsWith(prefix)) { return; }

	 //otherwise we start analysing the input.
	 // If the player calls start, we try to add the player to the game. by adding
	 //them to the players array
	 //at the index specified by their unique author id, this allows for quick
	 //referencing the player later.
	 if (msg.content.startsWith(prefix + 'START')) {
		  if (players[msg.author.id] !== undefined) {
				msg.channel.sendMessage(author + 'ERROR: Unable to start game. Perhaps you\'re already in it?');
		  } else {
				//the player object includes a channel, the discord server and channel they are connnected to
				//as well as their id, and the room that they are currently in. and any flags, that can determine the players state
				players[msg.author.id] = {id: author, channel: msg.channel, inventory: {}, room: relative('./lib/rooms/upperDeck/rm_bridge.js'), flags: {}};
				//then we send a start message to the player to bring them up to speed.
				msg.channel.sendMessage('All stellar contacts lost - Exiting Stealth Mode.\n - Reingaging Main Power Modules: Failed.\n - Engaging Backup Power Modules: Failed.\n - Engaging Emergency\nBridge Power: Error: Reserves Low.\nAttempting to wake Bridge Crew Unit ' + author + ':\n - Disengaging Cryosleep: Succes\n - Restoring Motor Controls: Success\n - Restoring Memory:\n Critical Failure\n      - Neural Recovery Network Destroyed...\n      - Checking Backups: No Backups...\n\nBridge Crew Unit ' + author + 'Revived without Memory. Well Shit.');
				//and add them to the starting room
				players[msg.author.id].room.enter(players[msg.author.id]);
		  }
		  return;
	 }
	 //checks to see if the player has a pre-existing save file,
	 //if for some reason they were removed from the game without getting a game over (i.e. server crash)
	 if (msg.content.startsWith(prefix + 'LOAD')) {
		  //files are saved based on the playes id, which will never change. almost anything relating to the player is.
		  fs.readFile('./lib/saves/' + msg.author.id + '.json', (err, data) => {
				if (err) {
					 msg.channel.sendMessage(author + 'Save file not found!');
				} else {
					 players[msg.author.id] = JSON.parse(data);
					 players[msg.author.id].room = relative(players[msg.author.id].room.name);
					 players[msg.author.id].channel = msg.channel;
					 msg.channel.sendMessage(author + 'Loaded successfully');
				}
		  });
		  return;
	 }
	 //if the player wants to save, write their player data to a file after checking that
	 //they are already in the game
	 if (msg.content.startsWith(prefix + 'SAVE')) {
		  if (players[msg.author.id] === undefined) {
				msg.channel.sendMessage(author + 'I can\'t save a game that you\'re not in, meatsack.');
		  } else {
				fs.writeFile('./lib/saves/' + msg.author.id + '.json', JSON.stringify(players[msg.author.id]), (err) => {
					 if (err) {
						  msg.channel.sendMessage(author + 'ERROR: ' + err);
					 } else {
						  msg.channel.sendMessage(author + 'Saved successfully');
					 }
				});
		  }
		  return;
	 }

	 //if the player hasn't joined the game, dont allow them to send commands
	 if (players[msg.author.id] === undefined) {
		  msg.channel.sendMessage(author + 'You forgot to start the game, meatsack.');
		  return;
	 }

	 var inputArr = msg.content.split(' ');

	 //check the room to see if it allows for the command given by the player. If not, check the defaults, if both fail, berate the player
	 if (_.has(players[msg.author.id].room.commands, inputArr[1])) {
		  players[msg.author.id].room.commands[inputArr[1]](players[msg.author.id], inputArr.slice(2).join(' '));
		  tick(players[msg.author.id]);
	 } else if (_.has(relative('./lib/rooms/default_commands.js').commands, inputArr[1])) {
		  relative('./lib/rooms/default_commands.js').commands[inputArr[1]](players[msg.author.id], inputArr.slice(2).join(' '));
		  tick(players[msg.author.id]);
	 }	else {
		  msg.channel.sendMessage(author + ' ' + inputArr[1] + ' Is not a recognised protocol. Are you sure you\'re all there?');
	 }
	 console.log(msg.content);
});


//computer turn, called for each player after they move
function tick(player){

}

//after establising how the bot will function, login.
bot.login(process.env.BOTTOKEN);
